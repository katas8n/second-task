import React, { Component } from 'react';
import Spinner from '../Spinner';

export default class Image extends Component {
    state = {
        isLoading: false,
        url: ''
    };
    componentDidMount() {
        fetch('https://source.unsplash.com/random').then(response => {
            console.log('[response.url]', response.url);
            this.setState({ isLoading: true, url: response.url });
        });
    }
    render() {
        const { url, isLoading } = this.state;

        return <> {isLoading ? <Spinner /> : <img src={url} />}</>;
    }
}
