import React from 'react';

import './Spiner.css';

const Spinner = () => {
    const loaderImg = ['fa', 'fa-spinner', 'loader'];

    return (
        <div className="spinner">
            <i className={loaderImg.join(' ')} />
        </div>
    );
};

export default Spinner;
